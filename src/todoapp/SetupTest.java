package todoapp;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.TestMethodOrder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public abstract class SetupTest {
	protected WebDriver driver;

    @Before
	public void startbrowserlogin() throws InterruptedException {
    System.setProperty("webdriver.chrome.driver", "chromedriver");
    driver = new ChromeDriver();
    driver.manage().deleteAllCookies();
	driver.manage().window().maximize();
	driver.get("https://qa-test.avenuecode.com/users/sign_in");
	driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.findElement(By.id("user_email")).sendKeys("nasirc93@gmail.com");
	driver.findElement(By.id("user_password")).sendKeys("Summer00");
	driver.findElement(By.name("commit")).click();
	Thread.sleep(1000);
    }
    
    @After
	public void teardown() {
		driver.quit();
	}

}
