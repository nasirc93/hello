package todoapp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FirstTest extends SetupTest {

	
	@Test
	public void testSubtasks() throws InterruptedException {
		driver.findElement(By.id("my_task")).click();
		Thread.sleep(2000);
		List <WebElement> subtasks = driver.findElements(By.xpath("//button[contains(.,'(0) Manage Subtasks')]"));
		subtasks.get(1).click();
		Thread.sleep(3000);
		assertTrue(driver.findElement(By.id("dueDate")).isDisplayed());
		driver.findElement(By.id("new_sub_task")).sendKeys("new sub task");
		driver.findElement(By.id("add-subtask")).click();
		Thread.sleep(1000);
		assertTrue(driver.findElement(By.xpath("//button[contains(.,'(1) Manage Subtasks')]")).isDisplayed());


	}
	@Test
	public void testbMytaskNameSize() throws InterruptedException {
		driver.findElement(By.id("my_task")).click();
		driver.findElement(By.id("new_task")).sendKeys("ne");
		driver.findElement(By.id("new_task")).sendKeys(Keys.ENTER);
		List <WebElement> tasksL = driver.findElements(By.tagName("a"));
		for (int ii = 0; ii< tasksL.size(); ii++) {
			if (tasksL.get(ii).getText().equals("ne")){
				assertTrue(false);
			}
		}
		Thread.sleep(1000);	
		}
	
		@Test
		public void testaMyTask() throws InterruptedException{
			assertTrue(driver.findElement(By.id("my_task")).isDisplayed());
			driver.findElement(By.id("my_task")).click();
			Thread.sleep(5000);
			assertEquals("https://qa-test.avenuecode.com/tasks", driver.getCurrentUrl());
			assertTrue(driver.findElement(By.id("new_task")).isDisplayed());
			Thread.sleep(2000);
			}
		
		

}
